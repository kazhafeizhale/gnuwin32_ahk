﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#include <runcmd>
msgbox,windows 命令支持管道和重定向

rtn := cmd.run("ls")
msgbox,% rtn

rtn := cmd.run("ls > ls.txt")
msgbox,% "请查看是否生成 ls.txt文件"

msgbox,linux 例子

rtn := gnuwin32("whoami")
msgbox,% "用户名：" rtn

rtn := gnuwin32("grep -rn gnuwin32")
msgbox,% "当前目录下文件查找  gunwin32结果" . rtn

rtn := gnuwin32("ps")
msgbox,% rtn

rtn := gnuwin32("ls")
msgbox,% rtn

rtn := gnuwin32("ls | grep ex")
msgbox,% rtn
