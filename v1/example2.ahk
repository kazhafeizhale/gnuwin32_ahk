﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#include <runcmd>
#include <log4ahk>
log.is_use_cmder := false
log.info("wait...")
sleep,2000
;windows 命令支持管道和重定向
log.info("cmd 命令")

rtn := cmd.run("ls","CP0")
log.info(rtn)

rtn := cmd.run("ls > ls.txt")
log.info("请查看是否生成 ls.txt文件")

log.info(cmd.run("echo 地球"))

rtn := cmd.run("ipconfig")
log.info("ip " rtn)

rtn := gnuwin32("whoami")
log.info("用户名：" rtn)

rtn := gnuwin32("grep -rn gnuwin32")
log.info("当前目录下文件查找  gunwin32结果：", rtn)

rtn := gnuwin32("ls")
log.info(rtn)


rtn := gnuwin32("ps", "CP0")
log.info(rtn)

rtn := gnuwin32("ls | grep ex")
log.info(rtn)